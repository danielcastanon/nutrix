$(function() {
  var inputQuantity = parseFloat($('.productQuantity').val());
  var packPrice = (parseFloat(($("h2.product-price").text()).replace(/[$,]+/g,""))).toFixed(2);
  var comparePrice = (parseFloat(($("h6.info.compare-price").text()).replace(/[$,]+/g,""))).toFixed(2);
  //codigo para comprar ahora
  var variantId = $("#productSelectt").val();
  $("#buyNowCartLink").attr("href", "/cart/"+variantId+":"+inputQuantity);
  $('#productSelectt').on('change', function (e) {
    var optionSelected = $("option:selected", this);
    variantId = this.value;
    $("#buyNowCartLink").attr("href", "/cart/"+variantId+":"+inputQuantity);
    console.log(variantId);
  });

  //calcular precio y checar si lleva descuentol, esto no afecta al backend, es puro visual
  $('#addbtn').on('click',function() {
    inputQuantity = inputQuantity + 1;
    $('.productQuantity').val(inputQuantity);
    $("#buyNowCartLink").attr("href", "/cart/"+variantId+":"+inputQuantity);
    $("h2.product-price").text("$"+(parseFloat(packPrice*inputQuantity).toFixed(2)));
    $(".priceProductBox").text("$"+(parseFloat(packPrice*inputQuantity).toFixed(2)));
    $("h6.info.compare-price").text("$"+(parseFloat(comparePrice*inputQuantity).toFixed(2)));
    if (inputQuantity >= 3 && ($(".discountCheck").text() == "3-15")) {
      $("h2.product-price").text("$"+(parseFloat(packPrice*inputQuantity*0.85).toFixed(2)));
      $("h2.product-priceVolume.3-15").text("$"+(parseFloat(packPrice*inputQuantity).toFixed(2)));
    } else {
      $("h2.product-priceVolume.3-15").text("");
    }
    if (inputQuantity >= 2 && ($(".discountCheck").text() == "2-15")) {
      $("h2.product-price").text("$"+(parseFloat(packPrice*inputQuantity*0.85).toFixed(2)));
      $("h2.product-priceVolume.2-15").text("$"+(parseFloat(packPrice*inputQuantity).toFixed(2)));
    } else {
      $("h2.product-priceVolume.2-15").text("");
    }
    if (inputQuantity == 3 && ($(".discountCheck").text() == "3-10")) {
      $("h2.product-price").text("$"+(parseFloat(packPrice*inputQuantity*0.90).toFixed(2)));
      $("h2.product-priceVolume.3-10").text("$"+(parseFloat(packPrice*inputQuantity).toFixed(2)));
    } else {
      $("h2.product-priceVolume.3-10").text("");
    }
    if (inputQuantity >= 4 && ($(".discountCheck").text() == "3-10")) {
      $("h2.product-price").text("$"+(parseFloat(packPrice*inputQuantity*0.85).toFixed(2)));
      $("h2.product-priceVolume.3-10").text("$"+(parseFloat(packPrice*inputQuantity).toFixed(2)));
    }
  }); 
  $('#removebtn').on('click',function() {
    inputQuantity = inputQuantity - 1;
    if (inputQuantity <= 0) {
      inputQuantity = 1;
    }
    $('.productQuantity').val(inputQuantity);
    $("#buyNowCartLink").attr("href", "/cart/"+variantId+":"+inputQuantity);
    $("h2.product-price").text("$"+(parseFloat(packPrice*inputQuantity).toFixed(2)));
    $(".priceProductBox").text("$"+(parseFloat(packPrice*inputQuantity).toFixed(2)));
    $("h6.info.compare-price").text("$"+(parseFloat(comparePrice*inputQuantity).toFixed(2)));

    //calcular precio a aquellos productos que cuenten con descuento por bulk
    if (inputQuantity >= 3 && ($(".discountCheck").text() == "3-15")) {
      $("h2.product-price").text("$"+(parseFloat(packPrice*inputQuantity*0.85).toFixed(2)));
      $("h2.product-priceVolume.3-15").text("$"+(parseFloat(packPrice*inputQuantity).toFixed(2)));
    } else {
      $("h2.product-priceVolume.3-15").text("");
    }
    if (inputQuantity >= 2 && ($(".discountCheck").text() == "2-15")) {
      $("h2.product-price").text("$"+(parseFloat(packPrice*inputQuantity*0.85).toFixed(2)));
      $("h2.product-priceVolume.2-15").text("$"+(parseFloat(packPrice*inputQuantity).toFixed(2)));
    } else {
      $("h2.product-priceVolume.2-15").text("");
    }
    if (inputQuantity == 3 && ($(".discountCheck").text() == "3-10")) {
      $("h2.product-price").text("$"+(parseFloat(packPrice*inputQuantity*0.90).toFixed(2)));
      $("h2.product-priceVolume.3-10").text("$"+(parseFloat(packPrice*inputQuantity).toFixed(2)));
    } else {
      $("h2.product-priceVolume.3-10").text("");
    }
    if (inputQuantity >= 4 && ($(".discountCheck").text() == "3-10")) {
      $("h2.product-price").text("$"+(parseFloat(packPrice*inputQuantity*0.85).toFixed(2)));
      $("h2.product-priceVolume.3-10").text("$"+(parseFloat(packPrice*inputQuantity).toFixed(2)));
    } else {
      $("h2.product-priceVolume.3-10").text("");
    }
  });

  //calcular precio y cantidad de productos al agregar o quitar en pagina de producto
  $('#addbtnn').on('click',function() {
    inputQuantity = inputQuantity + 1;
    $('.productQuantity').val(inputQuantity);
    $("h2.product-price").text("$"+(parseFloat(packPrice*inputQuantity).toFixed(2)));
    $(".priceProductBox").text("$"+(parseFloat(packPrice*inputQuantity).toFixed(2)));
    $("h6.info.compare-price").text("$"+(parseFloat(comparePrice*inputQuantity).toFixed(2)));
  });

  $('#removebtnn').on('click',function() {
    inputQuantity = inputQuantity - 1;
    if (inputQuantity <= 0) {
      inputQuantity = 1;
    }
    $('.productQuantity').val(inputQuantity);
    $("h2.product-price").text("$"+(parseFloat(packPrice*inputQuantity).toFixed(2)));
    $(".priceProductBox").text("$"+(parseFloat(packPrice*inputQuantity).toFixed(2)));
    $("h6.info.compare-price").text("$"+(parseFloat(comparePrice*inputQuantity).toFixed(2)));
  });

  $('.productDescriptionsCarousel').slick({
      adaptiveHeight: true,
      dots: true,
      cssEase: 'linear',
      arrows: false,
      appendDots: $(".dotsandarrows")
  });

  //botones para cambiar de slide de carrousel
  $('.firstSlick').on('click',function() {
    $('.productDescriptionsCarousel').slick('slickPrev');
  });

  $('.secondSlick').on('click',function() {
    $('.productDescriptionsCarousel').slick('slickNext');
  });

  $('.productDescriptionsCarousell').slick({
      adaptiveHeight: true,
      dots: true,
      cssEase: 'linear',
      arrows: true,
      appendDots: $(".deslizaMovil")
  });

  //sliders de descripciones de producto
  if ($(window).width() < 750) {
    $(".dropDescription").on('click',function() {
      $(".productDescription").slideToggle(300);
      $(".dropDescription").toggleClass("open");
    });
    $(".dropBeneficios").on('click',function() {
      $(".productBeneficios").slideToggle(300);
      $(".dropBeneficios").toggleClass("open");
    });
    $("#contenidosBar").on('click',function() {
      if (($('.productDescriptionsCarousell.mobile').is(':visible')) || ($('.imagesSistema').is(':visible'))) {
        $(".imagesSistema").slideUp(300);
        $(".productDescriptionsCarousell.mobile").slideUp(300);
        $(".miraContenidos").slideUp(300).removeClass("open");
        $("#contenidosBar").removeClass("open");
        $(".deslizaMovil").removeClass("open");
      } else {
        $(".deslizaMovil").addClass("open");
        $(".productDescriptionsCarousell.mobile").slideDown(300);
        $('.productDescriptionsCarousell.mobile').slick('setPosition');
        $(".miraContenidos").slideDown(300);
        $("#contenidosBar").slideDown("open");
        $("#contenidosBar").addClass("open");
      }
    });
    $(".miraContenidos").on('click',function() {
      $(".productDescriptionsCarousell.mobile").slideToggle("slow");
      $(".imagesSistema").slideToggle("slow");
      $(".miraContenidos").toggleClass("open");
    });
    $("#comoUsoButton").on('click',function() {
      $("#comoUso").slideToggle(300);
      $("#comoUsoButton").toggleClass("open");
    });
  };

   $("#landing-producto").on("click",function(){
      window.location.href = "https://nutrixmexico.com/pages/reto-30-dias";
    });
  //abir y cerrar carrito
  $(".cart-pop-exit").on('click',function() {
    $(".cart-overlay-background").fadeOut();
  });
  $("#cartOpenBtn").on('click',function() {
    $(".cart-overlay-background").fadeToggle();
  });
  //funcion para saber si algo esta visible
  $.fn.isInViewport = function() {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();
    return elementBottom > viewportTop && elementTop < viewportBottom;
  };

  //cargar el header chico cuando se scrollea para abajo
  $(document).scroll(function() {
   var value=$(document).scrollTop();/* <== here*/
    if ( value >= 127 ) {
      $("#site-header").addClass("up");
      $(".site-header img").addClass("up");
    } else {
      $("#site-header").removeClass("up");
      $(".site-header img").removeClass("up");
    }
  });

  //agregar zoom en hover cuando no es movil
  if ($(window).width() < 750) {

  } else {
    $('#top-product-image .featuredImage')
    .wrap('<span style="display:inline-block"></span>')
    .css('display', 'block')
    .parent()
    .zoom();
  }

  $('.infoNut, .infoNutClose').on('click',function() {
    $('.infoNutrimentalContainer').toggle();
  });
  
  
  
    $(".home-products-container").slick({
      dots: false,
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      autoplay: true,
      arrows: false,
      draggable: true,
      swipe: true,
      responsive: [{
      breakpoint: 750,
        settings: {
          slidesToShow: 1,
          infinite: true,
          centerMode: true,
          centerPadding: '100px'
        }
      }]
    });
    var stHeight = $('.slick-track').height();
    $('.slick-slide').css('height',stHeight + 'px' );


  
  

  if ($('span.count').length > 1){
  window._mfq = window._mfq || [];
  window._mfq.push(['activateFeedback', 'kWQj8GLWnUOGfANaz8GNsg']);
	}
});



//llamar a mouseflow
