/* ALL THE SCRIPTS IN THIS FILE ARE MADE BY KROWNTHEMES.COM --- REDISTRIBUTION IS NOT ALLOWED! */

$(window).bind("pageshow", function(event) {
if (event.originalEvent.persisted) {
      window.location.reload()
    }
});

$.ajaxSetup({ cache: false });

// CONTINUE WITH EVERYTHING ELSE

window.blockStickyHeader = false;

(function($) {

	$('#site-filters select:not(.styled)').each(function(){

		$(this).styledSelect({
			coverClass: 'regular-select-cover',
			innerClass: 'regular-select-inner'
		}).addClass('styled');

		$(this).parent().append($.themeAssets.arrowDown);

		if ( $(this).data('label') != '' ) {

			if ( $(this).find('option:selected').hasClass('default') ) {
				$(this).parent().find('.regular-select-inner').html($(this).data('label'));
			}

			$(this).on('change', function(e){
				if ( $(this).find('option:selected').hasClass('default') ) {
					$(this).parent().find('.regular-select-inner').html($(this).data('label'));
				}
			});

		}

	});

	$('.site-header').append('<span id="js-helpers"><span id="fix-me-header"></span><span id="fix-me-collection"></span></span>');

	// tab navigation

	$('a:not(.ach), button, span, input').on('focus', function(e){
   	$(this).addClass('hover');
	}).on('blur', function(e){
		$(this).removeClass('hover');
	});

	$(document).keyup(function(e) {
	  if (e.keyCode === 27) {
	  	$('.site-close-handle').trigger('click');
	  }
	});

	// We start with the newsletter code (it needs to be wrapped inside a $(window).load() event function in order to get the perfect timeout after the site has completely loaded

	window.CUBER = {

		// MAIN

		Nav: {

			$siteHeader: null,
			$siteNav: null,
			$siteOverlay: null,

			mount: function() {

				this.$siteHeader = $('#site-header');
				this.$siteNav = $('#site-nav--mobile');
				this.$siteOverlay = $('#site-overlay');

				$('#site-menu-handle').on('click focusin', (function(){

					if ( ! this.$siteNav.hasClass('active') ) {

						this.$siteNav.addClass('active');
						this.$siteNav.removeClass('show-filters').removeClass('show-cart').removeClass('show-search');

						this.$siteOverlay.addClass('active');

						$('.fixing-scroll-now .site-box-background').addClass('sidebar-move');
						$('body').addClass('sidebar-move');

					}

				}).bind(this));

				if ( $('#site-filter-handle').length > 0 ) {

					$('#site-filter-handle').on('click', (function(){

						if ( ! this.$siteNav.hasClass('active') ) {

							this.$siteNav.addClass('active');
							this.$siteNav.removeClass('show-cart').removeClass('show-search').addClass('show-filters');

							this.$siteOverlay.addClass('active');

							$('.fixing-scroll-now .site-box-background').addClass('sidebar-move');
							$('body').addClass('sidebar-move');

						}

					}).bind(this));

				}

				if ( $('#site-cart-handle').length > 0 && ! $('html').hasClass('ie9') ) {

					if ( $.themeCartSettings == 'overlay' ) {

						$('#site-cart-handle a').addClass('block-fade');

						$('#site-cart-handle a').on('click', (function(e){

							e.preventDefault();

							if ( ! this.$siteNav.hasClass('active') ) {

								this.$siteNav.addClass('active');
								this.$siteNav.removeClass('show-filters').removeClass('show-search').addClass('show-cart');

								this.$siteOverlay.addClass('active');

								$('.fixing-scroll-now .site-box-background').addClass('sidebar-move');
								$('body').addClass('sidebar-move');

							}

						}).bind(this));

					}

					$('#site-cart-handle').on('mouseenter focusis', function(){
						$(this).addClass('hover-in').removeClass('hover-out');
					}).on('mouseleave focusout', function(){
						$(this).addClass('hover-out').removeClass('hover-in');
					});

				}

				if ( $('#site-search-handle').length > 0 ) {

					$('#site-search-handle a').addClass('block-fade');

					$('#site-search-handle a').on('click', (function(e){

						e.preventDefault();

						if ( ! this.$siteNav.hasClass('active') ) {

							if ( $('html').hasClass('ie9') ) {
								document.location.href = '/search';
							} else {
								this.$siteNav.addClass('active');
								this.$siteNav.removeClass('show-filters').removeClass('show-cart').addClass('show-search');

								this.$siteOverlay.addClass('active');

								$('.fixing-scroll-now .site-box-background').addClass('sidebar-move');
								$('body').addClass('sidebar-move');

								$('form.search-bar input[type="search"]').focus();
							}

						}

					}).bind(this));

					$('#site-search-handle').on('mouseenter', function(){
						$(this).addClass('hover-in').removeClass('hover-out');
					}).on('mouseleave focusout', function(){
						$(this).addClass('hover-out').removeClass('hover-in');
					});

					$('a[href="#search"]').on('click', function(e){
						e.preventDefault();
						$('#site-search-handle a').trigger('click');
					})

				}

				$('.site-close-handle, #site-overlay').on('click', (function(){

					if ( this.$siteNav.hasClass('active') ) {

						this.$siteNav.removeClass('active');
						this.$siteOverlay.removeClass('active');

						$('.fixing-scroll-now .site-box-background').removeClass('sidebar-move');
						$('body').removeClass('sidebar-move');

					}

				}).bind(this));

				///

				$('.site-nav.style--classic .has-submenu').each(function(){
					$(this).on('mouseenter focusin', function(){
						$(this).find('.submenu').stop().slideDown(200);
						$('body').addClass('opened-submenu');
						$('body').addClass('opened-submenu-flag');
						$('.site-overlay').addClass('active');
						$(this).find('.submenu').attr('aria-expanded', 'true');
					}).on('mouseleave focusout', function(){
						$(this).find('.submenu').attr('aria-expanded', 'false');
						$(this).find('.submenu').stop().slideUp(200, function(){
							if ( ! $('body').hasClass('opened-submenu-flag') ) {
								$('body').removeClass('opened-submenu');
							}
						});
						$('.site-overlay').removeClass('active');
						$('body').removeClass('opened-submenu-flag');
					})
				})

				$('.site-nav.style--sidebar .has-submenu:not(.collections-menu)').each(function(){

					$(this).children('a').addClass('block-fade');
					$(this).children('a').on('click touchstart', function(e) {
						e.preventDefault();
					})

					$(this).children('a').on('click touchstart', function(e){

						e.preventDefault();
						var $parent = $(this).parent();

						if ( $parent.hasClass('active') ) {

							var tempBoNo = false;
							var tempBoHref = $(this).attr('href');

							if ( tempBoHref != '' ) {

								if ( tempBoHref.indexOf('#') >= 0 ) {

									tempBoNo = true;

									if ( tempBoHref.indexOf('#') == ( tempBoHref.length-1 ) ) {
										// nothing
									} else {
		  							$('.site-close-handle').trigger('click');
				    				document.location.href = tempBoHref;
									}

								} else if ( $(this).attr('target') == '_blank' ) {

									window.open(tempBoHref, '_blank');

								} else {

				    			$('body').fadeOut(200);
				    			setTimeout((function(){
				    				document.location.href = tempBoHref;
				    			}).bind(this), 200);
				    			e.preventDefault();

								}

							} else {
								tempBoNo = true;
							}

							if ( tempBoNo ) {
								$parent.find('.submenu').stop().slideUp(200);
								$parent.removeClass('active');
								$parent.find('.submenu').attr('aria-expanded', 'false');
							}

						} else {
							$parent.addClass('active');
							$parent.find('.submenu').stop().slideDown(200);
							$parent.find('.submenu').attr('aria-expanded', 'true');
						}

						$(this).removeClass('hover');

					})

				})

				$('.site-nav.style--sidebar .has-babymenu:not(.collections-menu)').each(function(){

					$(this).children('a').addClass('block-fade');
					$(this).children('a').on('click touchstart', function(e) {
						e.preventDefault();
					})

					$(this).children('a').on('click touchstart', function(e){

						e.preventDefault();
						var $parent = $(this).parent();

						if ( $parent.hasClass('active') ) {

							var tempBoNo = false;
							var tempBoHref = $(this).attr('href');

							if ( tempBoHref != '' ) {

								if ( tempBoHref.indexOf('#') >= 0 ) {

									tempBoNo = true;

									if ( tempBoHref.indexOf('#') == ( tempBoHref.length-1 ) ) {
										// nothing
									} else {
		  							$('.site-close-handle').trigger('click');
				    				document.location.href = tempBoHref;
									}

								} else if ( $(this).attr('target') == '_blank' ) {

									window.open(tempBoHref, '_blank');

								} else {

				    			$('body').fadeOut(200);
				    			setTimeout((function(){
				    				document.location.href =tempBoHref
				    			}).bind(this), 200);
				    			e.preventDefault();

								}

							} else {
								TempBoNo = true;
							}

							if ( tempBoNo) {
								$parent.removeClass('active');
								$parent.find('.babymenu').stop().slideUp(200);
								$parent.find('.babymenu').attr('aria-expanded', 'false');
							}

						} else {
							$parent.addClass('active');
							$parent.find('.babymenu').stop().slideDown(200);
							$parent.find('.babymenu').attr('aria-expanded', 'true');
						}

					})

				})

				$('.style--classic .babymenu').each(function(){

					var bestWidth = 0;
					$(this).parent().parent().css('display', 'block');

					$(this).find('a').each(function(){
						$(this).css('position', 'fixed');
						$(this).attr('data-width', $(this).outerWidth(true))
						if ( $(this).outerWidth() > bestWidth ) {
							bestWidth = $(this).outerWidth(true);
						}
						$(this).css({'position': 'static', 'width': '100%'});
					});

					bestWidth += 30;
					$(this).css('width', bestWidth);
					$(this).css('transform', 'translateX(-45%)');
					$(this).parent().parent().css('display', 'none');
					$(this).css('display', 'none');

				});

				$('.style--classic .has-babymenu').each(function(){
					$(this).on('mouseenter focusin', function(){
						$(this).find('.babymenu').stop().slideDown(200);
						$(this).find('.babymenu ul').attr('aria-expanded', 'true');
						$(this).css('zIndex', 9);
					}).on('mouseleave focusout', function(){
						$(this).find('.babymenu').stop().slideUp(200);
						$(this).find('.babymenu ul').attr('aria-expanded', 'false');
						$(this).css('zIndex', 1);
					});
				});

				///

				$('body').addClass('desktop--add-some-padding');

				$('.style--classic li.has-submenu[data-size]').each(function(){
					var menuSize = parseInt( $(this).data('size') )
					if ( menuSize > 15 ) {
						$(this).addClass('text-smallest');
					} else if ( menuSize > 10 ) {
						$(this).addClass('text-smaller');
					} else if ( menuSize > 5 ) {
						$(this).addClass('text-small');
					}
				});

				// end -- */

				if ( $('#site-header').hasClass('header-scroll') ) {

					if ( $('body').hasClass('template-index') ) {
						$('body').addClass('index-margin-fix');
					}

					window.lst = $(window).scrollTop();

					$('.site-nav.style--classic .submenu').css('top', $('.site-header').outerHeight())

					$(window).on('scroll.sticky-header', (function(){

						if ( ! window.blockStickyHeader ) {

			        var st = $(window).scrollTop();

			        if ( st < 0 || Math.abs(lst - st) <= 5 )
			          return;

			       	if ( st == 0) {

								this.$siteHeader.removeClass('animate');
								this.$siteHeader.removeClass('fix');
								this.$siteHeader.removeClass('ready');
								//$('body').css('paddingTop', 0);

			       	} else if ( st <= lst && ! this.$siteHeader.hasClass('fix') ) {

								this.$siteHeader.addClass('fix');
								//$('body').css('paddingTop', this.$siteHeader.outerHeight());
								setTimeout((function(){
									this.$siteHeader.addClass('ready');
								}).bind(this), 5);
								setTimeout((function(){
									this.$siteHeader.addClass('animate');
								}).bind(this), 25);

							} else if ( st > lst && this.$siteHeader.hasClass('animate') ) {

								this.$siteHeader.removeClass('animate');
								setTimeout((function(){
									this.$siteHeader.removeClass('fix');
									this.$siteHeader.removeClass('ready');
									//$('body').css('paddingTop', 0);
								}).bind(this), 105);

							}

							window.lst = st;

						}

					}).bind(this));

				}

			},

			unmount: function() {

				$('#site-menu-handle').off('click');
				$('#site-cart-handle a').off('click');
				$('#site-filter-handle').off('click');

				this.$siteNav.removeClass('active');
				this.$siteOverlay.removeClass('active');

				$('.fixing-scroll-now .site-box-background').removeClass('sidebar-move');
				$('body').removeClass('sidebar-move');

				$(window).off('scroll.sticky-header');

			}

		},

		// COLLECTION

		Collection: {

			$collectionGrid: null,
			$collectionNext: null,
			$collectionNextLink: null,

			mount: function() {

				if ( $('.box__paginate').length > 0 ) {

					this.$collectionGrid = $('#section-collection');
					this.$collectionNext = $('.box__paginate');
					this.$collectionNextLink = $('.box__paginate a');

					this.$collectionNextLink.append('<div class="preloader"><span>.</span><span>.</span><span>.</span></div>')

					this.$collectionNextLink.on('click', (function(e){

						this.$collectionNextLink.addClass('loading');
						var nextPageURL = this.$collectionNextLink.attr('href');

						$.ajax({
							url: nextPageURL
						}).done((function(data){

							this.$collectionNextLink.removeClass('loading');
							this.$collectionNext.before($(data).find('.site-box.box__collection'));
							//window.CUBER.Images.mount();

							var i = 0;
							this.$collectionGrid.find('.box__collection:not(.active)').each(function(){
								setTimeout((function(){
									$(this).addClass('active');
								}).bind(this), 100*i++);
							});

							if ( $(data).find('.site-box.box__paginate').length > 0 ) {
								this.$collectionNextLink.attr('href', $(data).find('.site-box.box__paginate a').attr('href'));
							} else {
								this.$collectionNext.remove();
							}

						}).bind(this));

						e.preventDefault();

					}).bind(this));

				}

				$(window).on('resize.collection-grid', window.debounce((function(){
					this._resizeCollection();
				}).bind(this), 300)).trigger('resize.collection-grid');
				this._resizeCollection();

				// empty grid fix

				var collectionSize =  parseInt($('.box__heading').data('size'));

				if ( collectionSize > 0 && collectionSize < 4 ) {
					for ( var i = 0; i < 4 - collectionSize; i++ ) {
						$('#section-collection').append('<div class="site-box box--small box--typo-small lap--hide box--no-padding box__collection active" />');
					}
				}

			},

			_resizeCollection: function() {


				if ( $('#fix-me-header').css('display') == 'block' && $('#fix-me-collection').css('display') == 'block' ) {

					var h = $(window).height() - $('.site-header').outerHeight();

					if ( h > 720 ) {

						//$('.mount-products .site-box.box__heading').attr('style', 'height:' + h + 'px !important; min-height: 0 !important;');
						$('.mount-products .site-box.box__collection_image').attr('style', 'height:' + h + 'px !important; min-height: 0 !important;');

					} else {

						h = 720;

						if ( $('.mount-products .site-box.box__collection_image').length > 0 ) {
							$('.mount-products .site-box.box__heading').attr('style', 'height:' + ( $(window).height() - $('.site-header').outerHeight() ) + 'px !important; min-height: 0 !important;');
							$('.mount-products .site-box.box__collection_image').attr('style', 'height:' + ( $(window).height() - $('.site-header').outerHeight() ) + 'px !important; min-height: 0 !important;');
						} else {

							//$('.mount-products .site-box.box__heading').attr('style', 'height:' + h + 'px !important; min-height: 0 !important;');
							//$('.mount-products .site-box.box__heading .site-box-content').css('marginTop', -$('.site-header').outerHeight());

						}


					}

					$('.mount-products .site-box.box__collection').each(function(){
						//$(this).attr('style', 'height:' + ( h / 2 ) + 'px !important');
					});

				} else {

					$('.mount-products .site-box.box__collection, .mount-products .site-box.box__heading').attr('style', '');

				}

			},

			unmount: function() {
				$(window).off('resize.collection-grid');
			}

		},

		// PRODUCT

		Product: {

			$productGallery: null,
			$productGalleryItem: null,
			$productGalleryIndex: null,

			$productCarousel: null,
			$productCarouselImgs: null,
			productFlkty: null,

			$productStuff: null,
			productStuffMove: false,

			mount: function($product) {

				// Product gallery

				var _this = {};
				$product.data('po', _this);

				_this.$productGallery = $product.find('.box__product-gallery');
				_this.$productGalleryItem = $product.find('.box__product-gallery .gallery-item');

				_this.$productGallery.append('<div class="gallery-index out-with-you"><span class="current">' + ( window.CuberProductImageIndex != undefined ? ( window.CuberProductImageIndex + 1 ) : 1 ) + '</span> / <span class="total">' + _this.$productGalleryItem.length + '</span></div>');
				_this.$productGalleryIndex = _this.$productGallery.find('.gallery-index .current');

				_this.$productCarousel = _this.$productGallery.children('.site-box-content');

				window.CUBER.Main._mountScrollMovers({
					'parent': _this.$productGallery,
					'items': $('.gallery-index, .site-sharing, .product-zoom')
				});

				window.CUBER.Main._mobileSharingInit();

				if ( _this.$productGallery.hasClass('scroll') ) {

					// init scrollable gallery

					$(window).on('scroll.product-gallery', (function() {

						if ( ! _this.$productCarousel.hasClass('flickity-enabled') ) {

							_this.$productGalleryItem.each((function(key, elm){

								if ( $(window).scrollTop() + $(window).height() > $(elm).offset().top + $(window).height() / 2 && ! $(elm).hasClass('current') ) {

									$(elm).addClass('current');
									_this.$productGalleryIndex.html($(elm).index() + 1);

								} else if ( $(window).scrollTop() + $(window).height() < $(elm).offset().top + $(window).height() / 2 && $(elm).hasClass('current') ) {

									$(elm).removeClass('current');
									_this.$productGalleryIndex.html($(elm).index());

								}

							}).bind(_this));

						}

					}).bind(_this)).trigger('scroll.product-gallery');

				}

				// init sliding gallery (always, because it turns into this at responsive)

				_this.$productCarousel.flickity({
					cellSelector: '.gallery-item:not(.remove-from-flick)',
					initialIndex: window.CuberProductImageIndex != undefined ? window.CuberProductImageIndex : 0,
					wrapAround: true,
					prevNextButtons: false,
					pageDots: true,
					watchCSS: _this.$productGallery.hasClass('scroll') ? true : false,
					resize: true
				});

				window.CUBER.Scroll.mount();

				_this.productFlkty = _this.$productCarousel.data('flickity');
				_this.$productCarouselImgs = _this.$productCarousel.find('.gallery-item img');

				_this.$productCarousel.on('select.flickity', (function(){
					_this.$productGalleryIndex.html((_this.productFlkty.selectedIndex+1));
				}).bind(_this))

				if ( _this.$productGallery.hasClass('slider') ) {

					_this.$productGallery.find('.gallery-index').append('<span class="icon-go go-prev">' + $.themeAssets.arrowRight + '</span><span class="icon-go go-next">' + $.themeAssets.arrowRight + '</span>');

					_this.$productGallery.find('.go-prev').on('click', (function(){
						_this.productFlkty.previous();
					}).bind(_this));

					_this.$productGallery.find('.go-next').on('click', (function(){
						_this.productFlkty.next();
					}).bind(_this));

				}

				// Product zoom

				if ( $('#product-zoom-in').length > 0 ) {

					$('body').append('<div id="product-zoomed-image"><img /><div id="product-zoom-out" class="product-zoom expand"><span class="zoom-out">' + $.themeAssets.iconZoom + '</span></div></div>');

					$('#product-zoom-in').on('click', (function(e){

						// animation out

						$('#section-product').find('.site-box.box__product-content').addClass('animate');
						setTimeout(function(){
							$('#section-product').find('.site-box.box__product-content').addClass('expand');
							$('body').addClass('kill-overflow');
						}, 10);

						$('#section-product').find('.site-box.box__product-gallery').stop().animate({'opacity': 0}, 200);
						$('#shopify-section-header, #shopify-section-footer, .site-sharing').stop().animate({'opacity': 0}, 200);

						// object

						var $image = $('#product-zoomed-image img');
						$image.attr('src', $('.gallery-item').eq(parseInt(_this.$productGalleryIndex.html())-1).data('master'));

						setTimeout((function(){
							if ( $image[0].naturalWidth > 0 ) {
									this._productZoomMount($image);
							} else {
								$image.on('load', (function(){
									this._productZoomMount($image);
								}).bind(this))
							}
						}).bind(this), 200);

					}).bind(this));

					$('#product-zoom-out').on('click', (function(e){

						setTimeout(function(){

							// animation out

							$('#section-product').find('.site-box.box__product-content').removeClass('expand');
							$('body').removeClass('kill-overflow')
							setTimeout(function(){
								$('#section-product').find('.site-box.box__product-content').removeClass('animate');
							}, 400);

							$('#section-product').find('.site-box.box__product-gallery').stop().animate({'opacity': 1}, 200);
							$('#shopify-section-header, #shopify-section-footer, .site-sharing').stop().animate({'opacity': 1}, 200);

						}, 150);

						// object

						this._productZoomUnmount();

					}).bind(this));

				}

				// Add to cart (& ajax)

				if ( ! $product.find('.product--section').hasClass('onboarding-true') ) {
					this.initProductForm($product);
				}

				if ( $product.find('.product--add-to-cart-form').length > 0 && $product.find('.product--add-to-cart-form').data('type') == 'overlay' && ! $('html').hasClass('ie9') ) {

					var $form = $product.find('.product--add-to-cart-form form'),
					  	$submitText = $form.find('.add-to-cart-text'),
					  	$submitButton = $form.find('button[type="submit"]'),
					  	$cartCount = $('.cart-menu .count'),
					  	$cartQty = $product.find('.quantity-selector');

			  	$form.submit(function(e){

			  		e.preventDefault();

			  		var oldText = $submitText.html();
			  		$submitText.html('<span class="preloader"><span>.</span><span>.</span><span>.</span></span>');
			  		$submitButton.css('pointer-events', 'none');

			  		var itemName = $product.find('.product-title').text(),
			  				itemPrice = $product.find('.product-price').attr('data-price'),
			  				itemCurrency = $('meta[itemprop="priceCurrency"]').attr('content'),
			  				itemId = $product.find('.product--section').data('id');

			  		$.ajax({

			  			type: $form.prop('method'),
			  			url: $form.prop('action'),
			  			data: $form.serialize(),
			  			dataType: 'json',
			  			success: function(data){

			  				setTimeout(function(){
			  					$submitText.html(oldText);
			  					$submitButton.css('pointer-events', 'all');
			  				}, 500);

			  				$.ajax({
			  					url: '/cart',
			  					success: function(data) {

			  						if ( typeof fbq !== 'undefined' ) {
  										fbq('track', 'AddToCart', {
									      content_name: itemName,
									      content_ids: [itemId],
									      content_type: 'product_group',
									      value: itemPrice,
									      currency: itemCurrency
  										});
			  						}

			  						$('#site-cart .cart-items').html($(data).find('#site-cart .cart-items .cart-item'));
			  						$('#CartTotal').html($(data).find('#CartTotal').html());

					  				setTimeout(function(){
					  					$submitText.html(oldText);
					  					$submitButton.css('pointer-events', 'all');
					  				}, 500);

					  				if ( $cartQty.length > 0 ) {

				  						var qty = parseInt($cartQty.val());
				  						if ( qty == 1 ) {
												$('#site-cart .subtitle').html($('#site-cart .subtitle').data('added-singular').replace(/{{ count }}|count|{{count}}/g, qty));
				  						} else {
												$('#site-cart .subtitle').html($('#site-cart .subtitle').data('added-plural').replace(/{{ count }}|count|{{count}}/g, qty));
				  						}

					  					$cartCount.text(parseInt($cartCount.text()) + parseInt($cartQty.val()));

					  				} else {

					  					$cartCount.text(parseInt($cartCount.text()) + 1);
											$('#site-cart .subtitle').html($('#site-cart .subtitle').data('added-singular').replace(/{{ count }}|count|{{count}}/, 1));

					  				}

					  				$('.site-cart-handle a').trigger('click');

			  					}
			  				});

			  			},

			  			error: function(data) {

			  				alert(data.responseJSON.description);

			  				setTimeout(function(){
			  					$submitText.html(oldText);
			  					$submitButton.css('pointer-events', 'all');
			  				}, 100);

			  			}
			  		});

			  	});

				}

			},

			_productZoomMount: function( $image ) {

				$('#product-zoomed-image').css('display', 'block');
				$image.css({'left': 0, 'top': 0});

				$(window).on('mousemove.product-zoom', function(e) {
					e.preventDefault();
					window.clientX = e.clientX;
					window.clientY = e.clientY;
					var x = e.clientX * ( $(window).width() - $image.width() ) / $(window).width();
					var y = e.clientY * ( $(window).height() - $image.height() ) / $(window).height();

					$image.css({'left': x, 'top': y});
				});

				if ( $image[0].naturalWidth <= $image[0].naturalHeight ) {
					$image.addClass('portrait');
				} else {
					$image.addClass('landscape');
				}
				$image.data('ratio', $image[0].naturalWidth / $image[0].naturalHeight);

				$(window).on('resize.product-zoom', function(){

					var rf = $(window).width() > 768 ? 1 : 2;

					if ( $image.hasClass('portrait') ) {
						$image.css('width', $(window).width()*rf);
						$image.css('height', $(window).width()*rf / $image.data('ratio'));
					} else {

						$image.css('height', $(window).height()*rf);
						$image.css('width', $(window).height()*rf * $image.data('ratio'));

						if ( $image.width() < $(window).width() ) {
							$image.css('width', $(window).width()*rf);
							$image.css('height', $(window).width()*rf / $image.data('ratio'));
						}

					}

					var x = window.clientX * ( $(window).width() - $image.width() ) / $(window).width();
					var y = window.clientY * ( $(window).height() - $image.height() ) / $(window).height();

					//$image.css({'left': x, 'top': y});

				}).trigger('resize');

				$image.css('opacity', 1);

			},

			_productZoomUnmount: function() {

				$('#product-zoomed-image img').css('opacity', '0');

				setTimeout(function(){
					$(window).off('resize.product-zoom');
					$(window).off('mousemove.product-zoom');
					$('#product-zoomed-image img').attr('src', '');
					$('#product-zoomed-image').css('display', 'none');
				}, 300);

			},

			initProductForm : function( $product ) {

				var firstVariantEver = true;

				var productSingleObject = JSON.parse($product.find('.product-json')[0].innerHTML),
						productVariants = productSingleObject.variants;

				$product.find('form select.product-variants').on('change', (function(e){
					this._initProductVariantChange(false, productSingleObject, $product);
				}).bind(this));

				this._initProductVariantChange(true, productSingleObject, $product);

				if ( $product.find('.product--add-to-cart-form').hasClass('style--classic') ) {

					$product.find('select.product-variants:not(.styled)').each(function(){

						$(this).styledSelect({
						  coverClass: 'regular-select-cover',
						  innerClass: 'regular-select-inner'
						}).addClass('styled');

					 	$(this).parent().append($.themeAssets.arrowDown);

				 	 	$(this).on('focusin', function(){
				 	 		$(this).parent().addClass('focus');
				 	 	}).on('focusout', function(){
				 	 		$(this).parent().removeClass('focus');
				 	 	});

					});

					$product.find('.product-variant').removeClass('hidden');

					if ( window.disableProductVariants ) {
						$product.find('.product-variant').css('display', 'none');
					}

				} else {

					var i = 1;

					$product.find('.product-variant').each(function(){

						var color = window.returnColorVariant($(this).find('label').text()) ? true : false,
								varDOM = '<ul class="color-' + color + '">';

						$(this).find('.product-variants option').each(function(){
							varDOM += '<li' + ( $(this).is(':selected') ? ' class="active"' : '' ) + ' tabindex="0" data-text="' + $(this).val() + '"><span' + ( color ? ' style="background-color: ' + $(this).val().split(' ').pop() + '"' : '' ) + '>' + $(this).val() + '</span></li>';
						});

						varDOM += '</ul>';
						$(this).find('select').hide();
						$(this).append(varDOM);

						$(this).removeClass('hidden');

						$(this).find('ul li').on('click', function(){
							$(this).parent().parent().find('select').val($(this).find('span').text()).change();
						});

						$(this).find('ul li').on('keyup', function(e){
							if ( e.keyCode == 13 ) {
								$(this).parent().parent().find('select').val($(this).find('span').text()).change();
							}
						});

					});

					if ( window.disableProductVariants ) {
				 		$product.find('.product-variant').find('li').addClass('disabled');
					}

				}

				if ( productSingleObject.variants.length == 1 && productSingleObject.variants[0].title.indexOf('Default') >= 0 ) {
					$product.find('.product-variant').hide();
				}

			},

			_getProductVariant : function( $product ) {

				/* ----
					Get current variant
					---- */

				var optionArray = [];

				$product.find('form select.product-variants').each(function(){
					optionArray.push($(this).find(':selected').val())
				});

				return optionArray;

			},

			_initProductVariantChange : function(firstTime, productSingleObject, $product) {

				var variant = null,
					options = this._getProductVariant( $product );

				productSingleObject.variants.forEach(function(el){

					if ( $(el)[0].options.equals(options) ) {
						variant = $(el)[0];
					}

				});

				this._productSelectCallback(variant, $product, productSingleObject);

				if ( ! firstTime && $product.find('#productSelect option').length > 1 && ! $('body').hasClass('template-index') ) {
					this._updateProductVariantState(variant);
				}

			},

			_updateProductVariantState : function(variant) {

			   if (!history.pushState || !variant) {
			    return;
			  }

			  var newurl = window.location.protocol + '//' + window.location.host + window.location.pathname + '?variant=' + variant.id;
			  window.history.replaceState({path: newurl}, '', newurl);

			},

			_productSelectCallback : function(variant, $product, productSingleObject) {

				var _po = $product.data('po');

				var $addToCart = $product.find('.product--add-to-cart-form'),
						$addToCartButton = $addToCart.find('button[type="submit"]'),
			      $productPrice = $product.find('.product-price'),
			      $comparePrice = $product.find('.compare-price'),
			      $quantityElements = $product.find('.quantity-selector, label + .js-qty'),
			      $addToCartText = $product.find('.add-to-cart-text'),
			      $productGallery = $product.find('.box__product-gallery');

				if (variant) {

					// Set cart value id

					$product.find('#productSelect').find('option[value="' + variant.id + '"]').prop('selected', true);

					// Swipe to variant slide

					if ( variant.featured_image != null ) {

						var variantImg = $productGallery.find('.gallery-item[data-variant-img="' + variant.featured_image.id + '"]');

						if ( typeof VIG !== 'undefined' ) {

							VIG.switchImages(productSingleObject.id, variant.id, '.box__product-gallery .site-box-content');

							if ( $product.find('.box__product-gallery .site-box-content').hasClass('flickity-enabled') ) {

								setTimeout(function(){

									$product.find('.box__product-gallery .gallery-item').each(function(){
										$(this).removeClass('remove-from-flick');
										if ( $(this).css('display') == 'none' ) {
											$(this).addClass('remove-from-flick');
										}
									})

									_po.productFlkty.reloadCells();
									_po.productFlkty.reposition();

								}, 1000);

							}

						} else if ( $productGallery.hasClass('scroll') && variantImg.length > 0 && $(window).width() > 1024 ) {

							/*if ( firstVariantEver ) {

								//window.blockStickyHeader = true;
								firstVariantEver = false;
								//THIS$(window).scrollTop(variantImg.offset().top);
			 					setTimeout(function(){
			 						$product.find('.gallery-index .current').html(variantImg.data('index')+1)
			 					}, 10);

							} else {*/

								//window.blockStickyHeader = true;
								/*$('html, body').animate({'scrollTop': variantImg.offset().top}, 350, function(){
									//window.blockStickyHeader = false;
										$('.gallery-index .current').html(variantImg.data('index')+1)
								});*/

								if ( $(window).scrollTop() > 0 ) {
									$('html, body').stop().animate({'scrollTop': 0}, 150, function(){
										$(window).off('scroll.to-junk');
									})
								}

							//}

							variantImg.prependTo($product.find('.box__product-gallery .site-box-content'))

						} else if ( _po != undefined && variantImg.length > 0 ) {
							if ( _po.productFlkty != undefined && variantImg.data('index') != _po.productFlkty.selectedElement ) {
								_po.productFlkty.select(variantImg.data('index'));
							} else {
								window.CuberProductImageIndex = variantImg.data('index');
							}
						}

					}

					// Disable other variants

					if ( $addToCart.hasClass('style--minimal') ) {
						$product.find('.product-variant li').each(function(){
							$(this).removeClass('active');
							if ( variant.option1 == $(this).data('text') || variant.option2 == $(this).data('text') || variant.option3 == $(this).data('text') ) {
								$(this).addClass('active');
							}
						})
					}

					// Edit cart buttons based on stock

					var $variantQuantity = $product.find('.variant-quantity');

					if ( variant.inventory_management == 'shopify' || variant.inventory_management == 'amazon_marketplace_web' || variant.inventory_management == null ) {

						if (variant.available) {

							if ( variant.inventory_quantity == 1 ) {
								$variantQuantity.text(window.product_words_one_product);
							} else if ( variant.inventory_quantity <= 5 ) {
								$variantQuantity.text(window.product_words_few_products.replace( '{{ count }}', variant.inventory_quantity));
							} else {
								$variantQuantity.text('');
							}
							$quantityElements.prop('max', variant.inventory_quantity);

							if ( variant.inventory_management == null || variant.inventory_policy == 'continue' ) {
								$quantityElements.prop('max', 999);
			  			}

			      	if ( variant.inventory_management == null ) {
			          $variantQuantity.text('');
			      	}

			      	if ( variant.inventory_policy == 'continue' && variant.inventory_quantity <= 0 ) {
			          $variantQuantity.text(window.product_words_preorder);
			      	}

							$addToCartButton.removeClass('disabled').prop('disabled', false);
							$addToCartText.text(window.product_words_add_to_cart_button);
							$quantityElements.show();

						} else {

							$variantQuantity.text(window.product_words_no_products);
							$addToCartButton.addClass('disabled').prop('disabled', true);
							$addToCartText.text(window.product_words_sold_out_variant);
							$quantityElements.hide();

						}

						$quantityElements.on('keyup', function(){
							if ( $quantityElements.prop('max') != undefined && parseInt($quantityElements.val()) > parseInt($quantityElements.prop('max')) ) {
								$quantityElements.val($quantityElements.prop('max'));
							}
						});
					}

					// Update price

					$productPrice.html( theme.Currency.formatMoney(variant.price, window.shop_money_format) );
					$productPrice.attr( 'data-price', variant.price / 100 );
					if ( variant.compare_at_price > variant.price ) {
						$comparePrice.html(theme.Currency.formatMoney(variant.compare_at_price, window.shop_money_format)).show();
					} else {
						$comparePrice.hide();
					}

					// Update sku

					if ( $product.find('.variant-sku').length > 0 ) {
						if ( variant ) {
							$product.find('.variant-sku').text(variant.sku);
						} else {
							$product.find('.variant-sku').empty();
						}
					}

				} else {

					// Disable variant completely

					$addToCartButton.addClass('disabled').prop('disabled', true);
					$addToCartText.text(window.product_words_unavailable_variant);
					$quantityElements.hide();

				}

			},

			unmount: function($product) {
				var po = $product.data('po');
				$(window).off('scroll.product-gallery');
				$(window).off('resize.position-product-zoom');
				po.$productCarousel.off('scroll.flickity');
				po.$productCarousel.off('select.flickity');
				po.productFlkty.destroy();
				$('#product-zoom').off('click');
			}

		},

		Main: {

			$searchForm: null,
			$searchResults: null,
			$searchPreloader: null,
			$searchPagination: null,

			$body: null,
			$siteHeader: null,
			$siteFooter: null,

			$siteBoxes: null,

			_mountScrollMovers: function(props) {

				var $parent = props['parent'],
						parentOutOfFocus = false;

				setTimeout(function(){
					props['items'].removeClass('out-with-you');
				}, 1000);
				props['items'].addClass('animate-owy');

				$(window).on('scroll.scroll-movers', (function(){

					/*if ( ! parentOutOfFocus && $(window).scrollTop() + $(window).height() < $parent.offset().top ) {

						// before element
						props['items'].addClass('out-with-you');
						parentOutOfFocus = true;

					} else */if ( ! parentOutOfFocus && $(window).scrollTop() + $(window).height() > $parent.offset().top + $parent.height() ) {

						// after element
						props['items'].addClass('out-with-you');
						parentOutOfFocus = true;

					} else if ( parentOutOfFocus && $(window).scrollTop() + $(window).height() <= $parent.offset().top + $parent.height() ) {

						// within element
						parentOutOfFocus = false;
						props['items'].removeClass('out-with-you');

					}

				}).bind(this));

			},

			_mobileSharingInit: function() {

				$('.touchevents .site-sharing .icon').on('touchstart', function(e){

					e.preventDefault();
					var $parent = $(this).parent();

					if ( $parent.hasClass('hover') ) {
						$parent.removeClass('hover');
					} else {
						$parent.addClass('hover');
					}

				});

				$('.touchevents .site-sharing a').on('touchstart', function(e){
					$(this).parent().removeClass('hover');
				});

			},

			_resizeEverything: function() {

				this.$body.css('paddingTop', this.$siteHeader.outerHeight());

				if ( this.$body.hasClass('template-index') ) {

					if ( $('#fix-me-header').css('display') == 'block' ) {

						if ( $('.mount-slideshow').children('div').length > 0 ) {

							$('#shopify-section-home_slideshow').css('marginTop', -this.$siteHeader.outerHeight());
							$('.fix-me-with-margin').css('marginTop', this.$siteHeader.outerHeight());

						} else {

							$('.index-section:first-child .fix-me-with-margin').css('marginTop', -this.$siteHeader.outerHeight());
							$('.index-section:first-child .fix-me-with-height').css('height', $(window).height() - this.$siteHeader.outerHeight());

						}

					} else {

						if ( $('.mount-slideshow').children('div').length > 0 ) {

							$('#shopify-section-home_slideshow').css('marginTop', '0');
							$('.slideshow-item:first-child .site-box-content').css('marginTop', '0');

						}

					}

				} else {

					if ( $('#fix-me-header').css('display') == 'block' ) {

						$('.fix-me-with-margin').each((function(key, elm){
							if ( $(elm).outerHeight() < $(window).height() ) {
								$(elm).css('marginTop', -this.$siteHeader.outerHeight())
							} else {
								$(elm).css('marginTop', '0')
							}
						}).bind(this));

						$('.fix-me-with-height-hard').css({
							'height': $(window).height() - this.$siteHeader.outerHeight(),
							'min-height': $(window).height() - this.$siteHeader.outerHeight()
						});
						$('.fix-me-with-height').css({
							'min-height': $(window).height() - this.$siteHeader.outerHeight()
						});

					} else {

						$('.fix-me-with-margin').css('marginTop', '0');
						$('.fix-me-with-height, .fix-me-with-height-hard').attr('style', '');

					}

				}

				if ( $('.mount-product').length > 0 ) {

					$('.mount-product').each((function(key, elm){

						if ( $('#fix-me-header').css('content') == '"fix-gallery-item"' ) {
							$(elm).find('.gallery-item').css('height', ( $(window).height() - this.$siteHeader.outerHeight() ) * 0.8 );
						} else {
							//$('.gallery-item').css('height', $(window).height() - this.$siteHeader.outerHeight());
						}

						$(elm).find('.product--section').removeClass('sticky-because');
						if ( $(elm).find('.site-box.box__product-gallery').css('content') != '"fix-me-also"' && $(elm).find('.site-box.box__product-gallery').height() < $(elm).find('.site-box.box__product-content').height() ) {
							$(elm).find('.product--section').addClass('sticky-because');
						}

					}).bind(this));

					/*if ( $('.site-box.box__product-gallery').css('content') == '"fix-me-also"' ) {
						$('.site-box.box__product-gallery').css('height', $(window).height() - this.$siteHeader.outerHeight());
					}*/

				}

				$('.site-header.desktop-view--classic .submenu').css('top', $('.site-header').outerHeight());

				if ( $('#site-menu-handle').css('opacity') == '1' ) {
					$('.site-nav.style--sidebar a, #site-menu-handle').attr('tabIndex', 0);
				} else {
					$('.site-nav.style--sidebar a, #site-menu-handle').attr('tabIndex', -1);
				}

			},

			_animateEverything: function(firstAnimation) {

				var i = 0;

				this.$siteBoxes.each(function(){

					var vp = 0;

					/*if ( ! firstAnimation ) {

						if ( ( $(this).height() - 100 ) > $(window).height() / 2 ) {
							vp = 0;
						} else {
							vp = 0;
						}
					}*/

					if ( ! $(this).hasClass('active') && $(window).scrollTop() + $(window).height() > $(this).offset().top + vp ) {

						i++;

						setTimeout((function(){
							$(this).addClass('active');
						}).bind(this), ( 100 * i ) );

					}

				});

				if ( ! this.$siteFooter.hasClass('active') && $(window).scrollTop() + $(window).height() > this.$siteFooter.offset().top + 150 ) {

					this.$siteFooter.addClass('active');
					this.$siteFooter.find('.site-box').addClass('active');

				}

			},

			mount: function() {

    		$('.rte').fitVids();

    		this.$siteBoxes = $('.site-box:not(.footer-box)');
    		this.$siteFooter = $('.site-footer');

    		if ( ! $('body').hasClass('ie9') ) {

					this._animateEverything(true);
	    		$(window).on('scroll.fade-animation', (function(e){
	 					this._animateEverything(false);
	    		}).bind(this));

	    	}

    		$('html.no-touchevents a[href]').each(function(){

	    		$(this).on('click', function(e){

	    			if ( ( e.ctrlKey || e.metaKey ) || $(this).attr('target') == '_blank' || $(this).hasClass('video-lightbox') || $(this).hasClass('lightbox') || $(this).hasClass('block-fade') || $(this).attr('href') == '' || $(this).attr('href').indexOf('#') >= 0 ) {
	    				//
	    			} else {

	    				$(this).off('click');

		    			$('body').fadeOut(200);
		    			setTimeout((function(){
		    				document.location.href = $(this).attr('href');
		    			}).bind(this), 200);
		    			e.preventDefault();
	    			}

		    	});

	    	});

				//

				this.$body = $('body');
				this.$siteHeader = $('#site-header');

				$(window).on('resize', window.debounce((function(){
					this._resizeEverything();
				}).bind(this), 300));
				this._resizeEverything();

				setTimeout((function(){
					this._resizeEverything();
				}).bind(this), 1000);

				if ( $('.logo-img').length > 0 && ! $('.logo-img img')[0].naturalWidth > 0 ) {
					$('.logo-img img').on('load', function(){
						window.CUBER.Main._resizeEverything();
					})
				}

				this.$searchForm = $('.search-bar:not(.no-ajax)');
				this.$searchResults = $('#search-results');
				this.$searchPreloader = $('#site-search .preloader');

				this.$searchForm.find('input[type="search"]').on('keyup', window.debounce((function(){
					this._ajaxSearchForm();
				}).bind(this), 300));

				this.$searchForm.submit((function(e){
					e.preventDefault();
					this._ajaxSearchForm();
				}).bind(this));

				// tabs & toggles

				if ( $('.krown-tabs').data('design') == 'toggles' ) {

		      $('.krown-tabs').each(function(){

		        var $titles = $(this).children('.titles').children('h5'),
		            $contents = $(this).find('.contents').children('div'),
		            i = 0;

		        $titles.each(function(){

		          $contents.eq(i++).insertAfter($(this));

		          $(this).on('click', function(){

		            var $toggle = $(this).next('.tab');

		            if ( ! $(this).hasClass('opened') ) {
		              $(this).addClass('opened');
		              $toggle.stop().slideDown(200);
		            } else {
		              $(this).removeClass('opened');
		              $toggle.stop().slideUp(200);
		            }

		          });

		        });

		        $(this).find('.contents').remove();

		      });

		    } else {

		      $('.krown-tabs').each(function(){

		        var $titles = $(this).children('.titles').children('h5'),
		        $contents = $(this).children('.contents').children('div'),
		        $openedT = $titles.eq(0),
		        $openedC = $contents.eq(0);

		        $openedT.addClass('opened');
		        $openedC.stop().slideDown(0);

		        $titles.find('a').prop('href', '#').off('click');;

		        $titles.click(function(e){

		          $openedT.removeClass('opened');
		          $openedT = $(this);
		          $openedT.addClass('opened');

		          $openedC.stop().slideUp(200);
		          $openedC = $contents.eq($(this).index());
		          $openedC.stop().delay(200).slideDown(200);

		          e.preventDefault();

		        });

		      });

		    }

			},

			_ajaxSearchForm: function() {

				this.$searchPreloader.css('opacity', 1);

	  		$.ajax({

	  			type: this.$searchForm.prop('method'),
	  			url: this.$searchForm.prop('action'),
	  			data: this.$searchForm.serialize(),
	  			success: (function(data){

	  				this.$searchResults.html($(data).find('.search-results.with-results'));
						this.$searchPreloader.css('opacity', 0);

						$(window).off('scroll.search-pagination');

						if ( $('.search-results').find('.next-page').length > 0 ) {
							this.$searchPagination = $('.search-results').find('.next-page');
							$(window).on('scroll.search-pagination', this._ajaxSearchPagination.bind(this));
						}

	  			}).bind(this)

	  		});

			},

			_ajaxSearchPagination: function() {

				if ( $(window).scrollTop() + $(window).height() >= this.$searchPagination.offset().top - 150 ) {

					this.$searchPreloader.css({'opacity': 1, 'top': 'auto', 'bottom': '-60px'});
					$(window).off('scroll.search-pagination');

					$.ajax({

						url: this.$searchPagination.attr('href'),
						success: (function(data){

							this.$searchResults.find('.next-page').remove();
							this.$searchResults.append($(data).find('.search-item'));

							if ( $(data).find('.search-results .next-page').length > 0 ) {
								this.$searchResults.append($(data).find('.search-results .next-page'));
								this.$searchPagination = $('.search-results').find('.next-page');
								$(window).on('scroll.search-pagination', this._ajaxSearchPagination.bind(this));
							}

							this.$searchPreloader.css({'opacity': 0, 'top': '20px', 'bottom': 'auto'});

						}).bind(this)

					})

				}

			},

			unmount: function() {
				$(window).off('scroll.scroll-movers');
			}

		},

		Contact: {

			mount: function($elm) {

				if ( $elm.find('.contact-map-holder').length > 0 ) {

					if ( $elm.find('.contact-map-object').data('address') != '' ) {

						if ( typeof google !== 'undefined' ) {
							this._createMap($elm.attr('id'), $elm.find('.contact-map-object'), $elm.find('.contact-map-address-holder'));
						} else {

							if ( window.loadingGoogleMapsScript ) {
								$elm.ti = setInterval((function($elm){
									if ( typeof google !== 'undefined' ) {
										clearInterval($elm.ti);
										this._createMap($elm.attr('id'), $elm.find('.contact-map-object'), $elm.find('.contact-map-address-holder'));
									}
								}).bind(this, $elm), 100);

							} else {

								window.loadingGoogleMapsScript = true;
								$.getScript('https://maps.googleapis.com/maps/api/js?v=3&key='+ $elm.find('.contact-map-holder').data('key')).done((function(){
									this._createMap($elm.attr('id'), $elm.find('.contact-map-object'), $elm.find('.contact-map-address-holder'));
								}).bind(this));

							}
						}

					}

				}

			},

			_createMap: function(id, $mapInsert, $mapAddress) {

				$mapInsert.attr('id', 'contact-map-' + id);

				var coordsKey = 'map-coords-' + $mapInsert.attr('id'),
						coordsStorage = localStorage.getItem(coordsKey) != null ? JSON.parse(localStorage.getItem(coordsKey)) : null,
						mapLat = 0,
						mapLong = 0;

				if ( coordsStorage != null && coordsStorage['address'] == $mapInsert.data('address') ) {
					mapLat = coordsStorage['lat'];
					mapLong = coordsStorage['long'];
				}

	     	var geocoder, map, address;

    		geocoder = new google.maps.Geocoder();
    		address = $mapInsert.data('address');

		    var mapOptions = {
		      zoom: $mapInsert.data('zoom'),
		      center: new google.maps.LatLng(mapLat, mapLong),
		      streetViewControl: false,
		      scrollwheel: true,
		      panControl: false,
		      mapTypeControl: false,
		      overviewMapControl: false,
		      zoomControl: false,
		      draggable: true,
		      styles: $mapInsert.data('style') == 'light' ? window.lightMapStyle : window.darkMapStyle
		    };

		    map = new google.maps.Map(document.getElementById($mapInsert.attr('id')), mapOptions);

		    if ( mapLat != 0 || mapLong != 0 ) {

			    var markerOptions = {
	          position: new google.maps.LatLng(mapLat, mapLong),
	          map: map,
	          title: address
			    }

			    if( $mapInsert.data('marker') != 'none' ) {
			    	markerOptions['icon'] = {
		          url: $mapInsert.data('marker'),
		          scaledSize: new google.maps.Size(60, 60)
			      }
			    }

			    $mapAddress.find('a').attr('href', 'http://www.google.com/maps/place/' + mapLat + ',' + mapLong);
			    var contentString = $mapAddress.html();
	        var infowindow = new google.maps.InfoWindow({
	          content: contentString
	        });

          var marker = new google.maps.Marker(markerOptions);
	        marker.addListener('click', function() {
	          infowindow.open(map, marker);
	          if ( $(window).width() < 480 ) {
	          	$('.template-page-contact .box__heading .title').css('marginTop', 50);
	          } else if ( $(window).width() < 768 ) {
	          	$('.template-page-contact .box__heading .title').css('marginTop', 100);
	          }
	        });

          if ( $(window).width() > 768 ) {
						map.panBy(-150, 150);
          } else {
						map.panBy(-75, 75);
          }

		    } else {

			    if ( geocoder ) {

			    	geocoder.geocode( { 'address': address }, function(results, status){

			    		if (status == google.maps.GeocoderStatus.OK) {
			          if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {

			          	map.setCenter(results[0].geometry.location);

							    var markerOptions = {
					          position: results[0].geometry.location,
					          map: map,
					          title: address
							    }

							    if( $mapInsert.data('marker') != 'none' ) {
							    	markerOptions['icon'] = {
						          url: $mapInsert.data('marker'),
						          scaledSize: new google.maps.Size(60, 60)
							      }
							    }

							    $mapAddress.find('a').attr('href', 'http://www.google.com/maps/place/' + results[0].geometry.location.lat() + ',' + results[0].geometry.location.lng());
							    var contentString = $mapAddress.html();
					        var infowindow = new google.maps.InfoWindow({
					          content: contentString
					        });

			            var marker = new google.maps.Marker(markerOptions);
					        marker.addListener('click', function() {
					          infowindow.open(map, marker);
					        });

				          if ( $(window).width() > 768 ) {
										map.panBy(-150, 150);
				          } else {
										map.panBy(-75, 75);
				          }

			  					localStorage.setItem(coordsKey, JSON.stringify({
			  						'address': $mapInsert.data('address'),
			  						'lat': results[0].geometry.location.lat(),
			  						'long': results[0].geometry.location.lng()
			  					}));

			          } else {
			            alert("No results found for the given address");
			          }
			        } else {
			          console.log("Geocode was not successful for the following reason: " + status);
			        }

			    	});

		   		}

		   	}

			},

			unmount: function() {

			}

		},

		// SCROLL

		Scroll: {

			$body: null,
			$footer: null,

			mount: function() {

				if ( ! $('html').hasClass('csspositionsticky') ) {

					this.$body = $('body');
					this.$footer = $('#shopify-section-footer');

					// grid

					var scrollFixArray = [];

					if ( $(window).width() > 768 ) {

						$('.site-box-container.container--fullscreen.box--can-stick').each(function(){

							var $boxChildren = $(this).children('.site-box');

							if ( $boxChildren.length == 2 ) {

								if ( $(this).children('.site-box[data-order="0"]').outerHeight(true) != $(this).children('.site-box[data-order="1"]').outerHeight(true) ) {

									var $bigChild = null,
											$smallChild = null;

									if ( $(this).children('.site-box[data-order="0"]').outerHeight(true) > $(this).children('.site-box[data-order="1"]').outerHeight(true) ) {
										$bigChild = $(this).children('.site-box[data-order="0"]');
										$smallChild = $(this).children('.site-box[data-order="1"]');
									} else if ( $(this).children('.site-box[data-order="1"]').outerHeight(true) > $(this).children('.site-box[data-order="0"]').outerHeight(true) ) {
										$bigChild = $(this).children('.site-box[data-order="1"]');
										$smallChild = $(this).children('.site-box[data-order="0"]');
									}

									scrollFixArray.push({
										'parent': $(this),
										'big-child': $bigChild,
										'small-child': $smallChild
									});

								}

							} else if ( $boxChildren.length == 1 ) {

								if ( $(this).children('.site-box[data-order="0"]').outerHeight(true) > $(window).height() ) {

									scrollFixArray.push({
										'parent': $(this),
										'big-child': $(this).children('.site-box[data-order="0"]'),
										'small-child': null
									});

								}

							}

						});

					}

					scrollFixArray.forEach(function(obj){

						obj['parent'].removeClass('fixing-scroll-now');
							obj['big-child'].css({'position': 'relative', 'top': '0', 'bottom': 'auto', 'marginLeft': '0'});

						if ( obj['small-child'] != null ) {
							obj['small-child'].css({'position': 'relative', 'top': '0', 'bottom': 'auto', 'marginLeft': '0'});
							obj['small-child'].removeClass('ok-scroll');
						}


					});

					$(window).off('scroll.scroll-fix');

					scrollFixArray.forEach(function(obj) {

						if ( $(window).scrollTop() + $(window).height() >= obj['parent'].offset().top + obj['parent'].outerHeight() ) {

							if ( obj['small-child'] != null  ) {
								obj['small-child'].css({'position': 'absolute', 'bottom': 0, 'top': 'auto' });
								if ( obj['small-child'].attr('data-order') == '1' ) {
									obj['small-child'].css('marginLeft', '50%');
								}
							}

							if ( obj['big-child'].attr('data-order') == '1' ) {
								obj['big-child'].css('marginLeft', '50%');
							}

						}

					})

					$(window).on('scroll.scroll-fix', (function(){

						if ( $(window).scrollTop() >= 0 ) {

							scrollFixArray.forEach(function(obj){

								if ( $(window).scrollTop() >= obj['parent'].offset().top && $(window).scrollTop() + $(window).height() < obj['parent'].offset().top + obj['parent'].outerHeight() && ! obj['parent'].hasClass('fixing-scroll-now') ) {

									obj['parent'].addClass('fixing-scroll-now');

									if ( obj['small-child'] != null ) {

										obj['small-child'].css({'position': 'fixed', 'top': 0, 'bottom': 'auto'});

										if ( obj['small-child'].attr('data-order') == '1' ) {
											obj['small-child'].css('marginLeft', '50%');
										}

										if ( obj['small-child'].height() > $(window).height() ) {
											obj['small-child'].addClass('ok-scroll');
										}

									}

									if ( obj['big-child'].attr('data-order') == '1' ) {
										obj['big-child'].css('marginLeft', '50%');
									}

								}

								if ( $(window).scrollTop() + $(window).height() >= obj['parent'].offset().top + obj['parent'].outerHeight() && obj['parent'].hasClass('fixing-scroll-now') ) {

									obj['parent'].removeClass('fixing-scroll-now');

									if ( obj['small-child'] != null ) {
										obj['small-child'].removeClass('ok-scroll');
										obj['small-child'].css({'position': 'absolute', 'bottom': 0, 'top': 'auto' });
									}

								}

								if ( $(window).scrollTop() < obj['parent'].offset().top && obj['parent'].hasClass('fixing-scroll-now') ) {

									obj['parent'].removeClass('fixing-scroll-now');
									obj['big-child'].css('marginLeft', '0');

									if ( obj['small-child'] != null ) {
										obj['small-child'].css({'position': 'relative', 'top': '0', 'bottom': 'auto', 'marginLeft': '0'});
									}


								}
								if ( obj['small-child'] != null && obj['small-child'].height() > $(window).height() && obj['small-child'].hasClass('ok-scroll') ) {

									var x = obj['big-child'].height() - $(window).height(),
											y = $(window).height() - obj['small-child'].height(),
											z = $(window).scrollTop();

									//obj['small-child'].css('top', Math.ceil(z * y / x));

								}


							});

						}

					}).bind(this)).trigger('scroll.scroll-fix');

					$(window).off('resize.scroll-fix');
					$(window).on('resize.scroll-fix', window.debounce((function(){
						window.CUBER.Scroll.mount();
					}).bind(this), 250));

				}

			},

			unmount: function() {

				$(window).off('resize.scroll-fix');
				$(window).off('scroll.scroll-fix');

			}

		},

		// IMAGES

		Images: {

			boxImages: [],

			mount: function() {

				this.boxImages = [];

				if ( $('.box--product-image').length > 0 ) {
					$('.box--product-image:not(.on)').each((function(key, elm){

						$(elm).addClass('on');
						this.boxImages.push({
							'$elm': $(elm),
							'img': $(elm).children('img')[0],
							'srcset': $(elm).children('img').data('srcset')
						});

					}).bind(this));
				}

				$(window).on('resize.box-images', (function(){

					this.boxImages.forEach(function(entry){

	        	var desiredDensity = window.devicePixelRatio <= 2 ? window.devicePixelRatio : 2,
	        			desiredSize = entry['$elm'].width() * desiredDensity;

	        	if ( entry['img'].naturalWidth > 0 ) {

	        		if ( entry['$elm'].width() / entry['$elm'].height() < entry['img'].naturalWidth / entry['img'].naturalHeight ) {
	        			var desiredHeight = Math.ceil(( entry['$elm'].height() * desiredDensity ));
	        			desiredSize = desiredHeight * entry['img'].naturalWidth / entry['img'].naturalHeight;
	        		}

	        	}

						if ( desiredSize <= 480 ) {
							entry['$elm'].css('backgroundImage', 'url(' + entry['srcset'].small + ')');
						} else if ( desiredSize <= 960 ) {
							entry['$elm'].css('backgroundImage', 'url(' + entry['srcset'].medium + ')');
						} else if ( desiredSize <= 1440 ) {
							entry['$elm'].css('backgroundImage', 'url(' + entry['srcset'].large + ')');
						} else if ( desiredSize > 1440 ) {
							entry['$elm'].css('backgroundImage', 'url(' + entry['srcset'].huge + ')');
						}

					});

				}).bind(this)).trigger('resize.box-images');

			},

			unmount: function() {
				$(window).off('resize.box-images');
			}

		},

		Video: {

			mount: function() {

				if ( $('.video-lightbox').length > 0 ) {
					$('.video-lightbox').magnificPopup({
						type: 'iframe'
					});
				}

			},

			unmount: function() {
			}

		},

		Popup: {

			$popup: null,

			mount: function() {

				this.$popup = $('#shopify-section-popup');

				var show = this.$popup.find('.popup-content').data('show'),
						freq = this.$popup.find('.popup-content').data('freq'),
						enable = this.$popup.find('.popup-content').data('enable');

				if ( show > 0 && enable ) {
					setTimeout((function(){
						this._trigger(show, freq);
					}).bind(this), parseInt(show*1000));
				}

				this.$popup.find('.site-close-handle').on('click', (function(e){
					this._hide();
				}).bind(this));

				this.$popup.find('.popup-background').on('click', (function(e){
					this._hide();
				}).bind(this));

			},

			_show: function() {
				this.$popup.addClass('active');
			},

			_hide: function() {
				this.$popup.removeClass('active');
			},

			_trigger: function(show, freq) {

				var popupKey = 'popup-' + document.location.hostname,
						popupStorage = localStorage.getItem(popupKey) != null ? JSON.parse(localStorage.getItem(popupKey)) : null;

				if ( popupStorage != null ) {

					if ( popupStorage['show'] != show || popupStorage['freq'] != freq ) {

						this._refreshStorage(popupKey, show, freq);;

						// user saw the ad but settings are different - show it!
						this._show();

					} else {

						// user saw the ad so we need to check if he should see it again

						if ( freq == 'every' ) {

							if ( sessionStorage[popupKey] == null || sessionStorage[popupKey] != 'shown' ) {
								this._show();
								this._refreshStorage(popupKey, show, freq);;
							}

						} else {

							var shownAt = popupStorage['shown'],
									nowAt = new Date().getTime(),
									inBetween = Math.round((nowAt - shownAt) / 1000);

							if ( freq == 'day' && inBetween > 129600 ) {
								this._show();
								this._refreshStorage(popupKey, show, freq);;
							} else if ( freq == 'week' && inBetween > 907200 ) {
								this._show();
								this._refreshStorage(popupKey, show, freq);;
							}

						}

					}

				} else {

					this._refreshStorage(popupKey, show, freq);

					// user never saw the ad - show it!

					this._show();

				}

			},

			_refreshStorage: function(popupKey, show, freq) {

				localStorage.setItem(popupKey, JSON.stringify({
					'show': show,
					'freq': freq,
					'shown': new Date().getTime()
				}));

				sessionStorage[popupKey] = 'shown';

			},

			unmount: function() {
			}

		},

		// SOCIAL

		Social: {

			$container: null,
			nameTwitter: '',
			nameInstagram: '',

			mount: function( $elm ) {

				this.$container = $elm.children('.site-box-container');
				this.nameTwitter = this.$container.data('twitter');
				this.nameInstagram = this.$container.data('instagram');

				var no = this.$container.data('posts'),
						noT = 0,
						noI = 0,
						socialArr = [];

				if ( this.nameTwitter != '' && this.nameInstagram != '' ) {
					noT = Math.ceil(no/2);
					noI = Math.floor(no/2);
				} else if ( this.nameTwitter != '' ) {
					noT = no;
				} else if ( this.nameInstagram != '' ) {
					noI = no;
				}

				if ( noI > 0 ) {

					var feed = new Instafeed({

						get: 'user',
						userId: this.nameInstagram.split('.')[0],
						accessToken: this.nameInstagram,
						limit: noI,
						resolution: 'standard_resolution',
						mock: true,
						sortBy: 'random',
						success: (function(obj) {

							obj.data.forEach(function(entry){

								socialArr.push({
									'type': 'instagram',
									'thumb': entry.images.standard_resolution.url,
									'link': entry.link,
									'caption': typeof entry.caption === 'object' && entry.caption !== null ? entry.caption.text : '',
									'likes': entry.likes.count,
									'time': new Date(entry.created_time*1000).toLocaleDateString('en-US'),
									'timestamp': parseInt(entry.created_time)
								});
							});

							this._pushFeed('instagram', socialArr);

						}).bind(this)

					});

					feed.run();

				} else {
					this._pushFeed('instagram', socialArr);
				}

				if ( noT > 0 ) {

					twitterFetcher.fetch({

					  'profile': {'screenName': this.nameTwitter},
					  'dataOnly': true,
					  'maxTweets': noT,
					  'dateFunction': function(date){
					  	return parse(date);
					  },
					  'customCallback': (function(obj){

					  	obj.forEach(function(entry){
					  		socialArr.push({
					  			'type': 'twitter',
					  			'text': entry.tweet,
					  			'timestamp': Date.parse(entry.timestamp)/1000
					  		})
					  	});

							this._pushFeed('twitter', socialArr);

					  }).bind(this)

					});

				} else {
					this._pushFeed('twitter', socialArr);
				}

			},

			_pushFeed: function(type, feed) {

				if ( type == 'twitter' ) {
					this.checkTwitter = true;
				} else if ( type == 'instagram' ) {
					this.checkInstagram = true;
				}

				if ( this.checkTwitter && this.checkInstagram ) {

					feed.sort(function(a, b){
						return a.timestamp - b.timestamp;
					}).reverse();

					feed.forEach((function(entry, key){

						var feedContent = '';

						if ( entry.type == 'twitter' ) {

							feedContent += '<div class="site-box-content">';
								feedContent += '<span class="icon" aria-hidden="true">' + $.themeAssets.iconTwitter + '</span>';
								feedContent += '<p>' + entry.text + '</p>';
								feedContent += '<a href="https://twitter.com/' + this.nameTwitter + '" target="_blank">@' + this.nameTwitter + '</a>';
							feedContent += '</div>';

						} else if ( entry.type == 'instagram' ) {

							feedContent += '<div class="site-box-content" style="background-image:url(' + entry.thumb + ')">';
								feedContent += '<a href="' + entry.link + '" target="_blank">';
									feedContent += '<span class="icon child" aria-hidden="true">' + $.themeAssets.iconInstagram + '</span>';
									feedContent += '<p class="caption child">' + entry.caption + '</p>';
									feedContent += '<div class="meta child">';
										feedContent += '<span class="likes">' + $.themeAssets.iconHeart + entry.likes + '</span>';
										feedContent += '<span class="time">' + entry.time + '</span>';
									feedContent += '</div>';
								feedContent += '</a>';
							feedContent += '</div>';

						}

						this.$container.find('.site-box[data-order="' + (key+1) + '"]')
							.removeClass('box__twitter').removeClass('box__instagram')
							.addClass('box__' + entry.type)
							.html(feedContent);

					}).bind(this));

				}

			},

			unmount: function() {
				//
			}

		},

		SplitSlider: {

			_mountFlickity: function() {

				$('.responsive-flickity').flickity({
					cellSelector: '.slideshow-item',
					wrapAround: true,
					prevNextButtons: false,
					pageDots: false,
					watchCSS: true,
					resize: true
				});

				var $slider = $('.box__slideshow-split'),
						flkty = $('.responsive-flickity').data('flickity'),
						currentI = 0,
						totalI = 100 / ( $slider.find('.slideshow-item').length - 1 ),
						$currentSlide = null,
						$currentSlideBlack = null;

				if ( $slider.find('.slider-meta').length <= 0 ) {

					$slider.find('.slider-meta').remove();

					$slider.append('<div class="slider-meta hide lap--show"><div class="slider-index"><span class="current">1</span> / <span class="total">' + sliderT + '</span></div><div class="slider-nav"><span class="go-prev">' + $.themeAssets.arrowRight + '</span><span class="go-next">' + $.themeAssets.arrowRight + '</span></div>' );

					$slider.find('.go-prev').on('click', (function(){
						flkty.previous();
					}).bind(this));

					$slider.find('.go-next').on('click', (function(){
						flkty.next();
					}).bind(this));

					$('.responsive-flickity').on('select.flickity', function(){
						$slider.find('.slider-index .current').html(flkty.selectedIndex + 1);
					});

					setTimeout(function(){
						$slider.find('.slider-meta').addClass('active');
					}, 1000);

				}

			},

			mount: function( flick ) {

				var $slider = $('.box__slideshow-split'),
						$slides = $slider.find('.slideshow-item'),
						$slidesMedia = $slider.find('.site-box-background-container').children('div'),
						slidesBlackArray = [];

						currentScroll = $(window).scrollTop(),

						sliderI = Math.min( Math.ceil(currentScroll / $(window).height()), $slides.length-1 );
						sliderJ = sliderI-1,
						sliderT = $slides.length;
				// mobile slider

				if ( flick ) {
					this._mountFlickity();
				}

				if ( $('.responsive-flickity').hasClass('flickity-enabled') ) {
					$slider.height($(window).height() - $('#site-header').outerHeight());
					$slider.addClass('remove-min-height');
				} else {
					$slider.css('height', 'auto');
					$slider.removeClass('remove-min-height');
				}

				// split

				$slidesMedia.each(function(key){

					if ( key > 0 ) {
						if ( key < sliderI ) {
							$(this).css('clip', 'rect(0 ' + Math.ceil($(window).width() / 2) + 'px ' + $(window).height() + 'px 0)');
						} else if ( key == sliderI ) {
							$(this).css('clip', 'rect(0 ' + Math.ceil($(window).width() / 2) + 'px ' + ( Math.ceil($(window).scrollTop() - $(window).height() * sliderJ ) ) + 'px 0)');
						} else {
							$(this).css('clip', 'rect(0 ' + Math.ceil($(window).width() / 2) + 'px 0 0)');
						}

					} else if ( key == 0 & flick ) {
                      $(this).css({
							'clip': 'rect(0 ' + Math.ceil($(window).width() / 2) + 'px 0 0)',
							'opacity': 0
						});

						$(this).addClass('clip-transition');
						setTimeout((function(){
							$(this).css({
								'clip': 'rect(0 ' + Math.ceil($(window).width() / 2) + 'px ' + $(window).height() + 'px 0)',
								'opacity': 1
							});
						}).bind(this), 10);

						setTimeout((function(){
							$(this).removeClass('clip-transition');
						}).bind(this), 650);

					}

					$(this).addClass('active');

					if ( $(this).find('.site-box-black-overlay').length <= 0 ) {
						$(this).append('<span class="site-box-black-overlay" />');
					}
					slidesBlackArray.push($(this).find('.site-box-black-overlay'));

				});

				$(window).on('scroll.split-slider', function(e){

					if ( currentScroll < $(window).scrollTop() ) {

						// next
						if ( $slides.eq(sliderI+1).length > 0 && $(window).scrollTop() + $(window).height() >= $slides.eq(sliderI+1).offset().top) {

							if ( sliderI != 0 ) {
								// finish scroll
								$slidesMedia.eq(sliderI).css('clip', 'rect(0 ' + Math.ceil($(window).width() / 2) + 'px ' + $(window).height() + 'px 0)');
								if ( slidesBlackArray[sliderJ] ) {
									slidesBlackArray[sliderJ].css('opacity', 0.5 );
								}
							}

							sliderJ = sliderI;
							sliderI++;
							down = true;

						} else if ( $(window).scrollTop() + $(window).height() >= $slider.height() && ! $slider.hasClass('back-to-normal') ) {
							$slider.addClass('back-to-normal');
								$slidesMedia.eq(sliderI).css('clip', 'rect(0 ' + Math.ceil($(window).width() / 2) + 'px ' + $(window).height() + 'px 0)');
						}

					} else {

						// prev
						if ( $slides.eq(sliderI).length > 0 && $slides.eq(sliderI-1).length > 0 && $(window).scrollTop() + $(window).height() < $slides.eq(sliderI).offset().top ) {

							// finish scroll

							var stupid = $slidesMedia.eq(sliderI).hasClass('obs') ? 1 : 0;

							$slidesMedia.eq(sliderI).css('clip', 'rect(0 ' + Math.ceil($(window).width() / 2) + 'px ' + stupid + 'px 0)');
							if ( slidesBlackArray[sliderJ] ) {
								slidesBlackArray[sliderJ].css('opacity', 0 );
							}

							sliderI--;
							sliderJ = sliderI-1;
							down = false;

						} else if ( $(window).scrollTop() + $(window).height() <= $slider.height() && $slider.hasClass('back-to-normal') ) {
							$slider.removeClass('back-to-normal');
						}

					}

					if ( ! $slider.hasClass('back-to-normal') ) {

						var scrollValue = Math.ceil($(window).scrollTop() - $(window).height() * sliderJ );

						var stupid = $slidesMedia.eq(sliderI).hasClass('obs') ? 1 : 0;

						$slidesMedia.eq(sliderI).css('clip', 'rect(0 ' + Math.ceil($(window).width() / 2) + 'px ' + Math.max(stupid, scrollValue) + 'px 0)');

						if ( slidesBlackArray[sliderJ] ) {
							slidesBlackArray[sliderJ].css('opacity', ( Math.ceil(scrollValue * 50 / $(window).height()) ) / 100 );
						}

						var scrollFactor = Math.round($(window).height() / 6);

						$slides.eq(sliderJ).find('.caption').css('transform', 'translateY(' + ( 0 - Math.ceil(scrollValue * ( scrollFactor*1 ) / $(window).height() ) ) + 'px)');
						$slides.eq(sliderJ).find('.title').css('transform', 'translateY(' + ( 0 - Math.ceil(scrollValue * ( scrollFactor*0.75 ) / $(window).height() ) ) + 'px)');
						$slides.eq(sliderJ).find('.subtitle').css('transform', 'translateY(' + ( 0 - Math.ceil(scrollValue * ( scrollFactor*0.5 ) / $(window).height() ) ) + 'px)');
						$slides.eq(sliderJ).find('.button').css('transform', 'translateY(' + ( 0 - Math.ceil(scrollValue * ( scrollFactor*0.25 ) / $(window).height() ) ) + 'px)');

						$slides.eq(sliderI).find('.caption').css('transform', 'translateY(' + ( Math.ceil(scrollValue * ( scrollFactor * 1) / $(window).height()) - ( scrollFactor * 1) ) + 'px)');
						$slides.eq(sliderI).find('.title').css('transform', 'translateY(' + ( Math.ceil(scrollValue * ( scrollFactor * 0.75) / $(window).height()) - ( scrollFactor * 0.75) ) + 'px)');
						$slides.eq(sliderI).find('.subtitle').css('transform', 'translateY(' + ( Math.ceil(scrollValue * ( scrollFactor * 0.5) / $(window).height()) - ( scrollFactor * 0.5) ) + 'px)');
						$slides.eq(sliderI).find('.button').css('transform', 'translateY(' + ( Math.ceil(scrollValue * ( scrollFactor * 0.25) / $(window).height()) - ( scrollFactor * 0.25) ) + 'px)');

					}

					currentScroll = $(window).scrollTop();

				}).trigger('scroll.split-slider');

				$(window).on('resize.split-slider', window.debounce((function(){

					this.unmount();
					this.mount(false);

				}).bind(this), 250));

			},

			unmount: function() {
				$(window).off('scroll.split-slider');
			}

		}

	}

	$(document).on('ready', function(){

		window.CUBER.Nav.mount();
		window.CUBER.Main.mount();
 		window.CUBER.Scroll.mount();

		if ( $('.mount-social').length > 0 ) {
			$('.mount-social').each(function(){
				window.CUBER.Social.mount($(this));
			})
		}

		if ( $('.box__slideshow-split').length > 0 ) {
			window.CUBER.SplitSlider.mount(true);
		}

		if ( $('.mount-product').length > 0 ) {
			$('.mount-product').each(function(){
				window.CUBER.Product.mount($(this));
			});
		}
		if ( $('body').hasClass('template-collection') ) {
			window.CUBER.Collection.mount();
		}
		if ( $('body').hasClass('template-page-contact') || ( $('body').hasClass('template-index') ) && $('.mount-map').length > 0 ) {
			$('.mount-map').each(function(){
				window.CUBER.Contact.mount($(this));
			});
		}

		//window.CUBER.Images.mount();
		window.CUBER.Video.mount();

		window.CUBER.Popup.mount();

		// Section events

		// select

		$(document).on('shopify:section:select', function(e){

	 		var $section = $(e.target);

	 		if ( $section.hasClass('mount-header') ) {

	 			if ( $section.find('#site-header').hasClass('style--sidebar' ) || $section.find('#site-header').hasClass('style--fullscreen' ) ) {
	 				if ( ! $section.find('#site-nav').hasClass('active') ) {
	 					$('#site-menu-handle a').trigger('click');
	 				}
	 			}

	 		}

	 		if ( $('#site-header').hasClass('desktop-view--minimal') && $('#fix-me-header').css('display') == 'none' ) {
	 			setTimeout(function(){
	 				$('html, body').stop().animate({'scrollTop': $section.offset().top}, 0);
	 			}, 400);
	 		}

	 		if ( $section.hasClass('mount-popup') ) {
	 			window.CUBER.Popup._show();
	 		}

	 	});

		// deselect

		$(document).on('shopify:section:deselect', function(e){

	 		var $section = $(e.target);

	 		if ( $section.hasClass('mount-header') ) {

	 			if ( $section.find('#site-header').hasClass('style--sidebar' ) || $section.find('#site-header').hasClass('style--fullscreen' ) ) {
	 				if ( $section.find('#site-nav').hasClass('active') ) {
	 					$('#site-menu-handle a').trigger('click');
	 				}
	 			}

	 		}

	 		if ( $section.hasClass('mount-popup') ) {
	 			window.CUBER.Popup._hide();
	 		}


		});

		// load

		$(document).on('shopify:section:load', function(e){

	 		var $section = $(e.target);

	 		if ( $section.hasClass('mount-header') ) {
	 			window.CUBER.Nav.mount();
	 		}
	 		if ( $section.hasClass('mount-images') ) {
	 			//window.CUBER.Images.mount();
	 		}
	 		if ( $section.hasClass('mount-video') ) {
				window.CUBER.Video.mount();
	 		}
	 		if ( $section.hasClass('mount-social') ) {
				window.CUBER.Social.mount($section);
	 		}
	 		if ( $section.hasClass('mount-slideshow') && $section.find('.box__slideshow-split').length > 0 ) {
	 			window.CUBER.SplitSlider.mount(true);
	 			$(window).scrollTop(0);
	 		}
	 		if ( $section.hasClass('mount-product') ) {
				window.CUBER.Product.mount($section);
	 		}
	 		if ( $section.hasClass('mount-map') ) {
				window.CUBER.Contact.mount($section);
	 		}

	 		if ( $section.hasClass('mount-popup') ) {
	 			window.CUBER.Popup.mount();
	 		}

	 		window.CUBER.Main.mount();
	 		window.CUBER.Scroll.mount();

		});

		// unload

		$(document).on('shopify:section:unload', function(e){

	 		var $section = $(e.target);

	 		if ( $section.hasClass('mount-header') ) {
	 			window.CUBER.Nav.unmount();
	 		}
	 		if ( $section.hasClass('mount-images') ) {
	 			window.CUBER.Images.unmount();
	 		}
	 		if ( $section.hasClass('mount-video') ) {
	 			window.CUBER.Video.unmount();
	 		}
	 		if ( $section.hasClass('mount-social') ) {
				window.CUBER.Social.unmount();
	 		}
	 		if ( $section.hasClass('mount-slideshow') && $section.find('.box__slideshow-split').length > 0 ) {
	 			window.CUBER.SplitSlider.unmount();
	 		}
	 		if ( $section.hasClass('mount-product') ) {
				window.CUBER.Product.unmount($section);
	 		}
	 		if ( $section.hasClass('mount-map') ) {
				window.CUBER.Contact.unmount();
	 		}

	 		window.CUBER.Main.unmount();
	 		window.CUBER.Scroll.unmount();

		});

		// blocks

		$(document).on('shopify:block:select', function(e){

			var $block = $(e.target);
			if ( $block.hasClass('slideshow-item') ) {
				if ( $('.responsive-flickity').hasClass('flickity-enabled') ) {
					$('.responsive-flickity').data('flickity').select($block.index())
				} else {
					$(window).scrollTop($block.offset().top);
				}
	 			$block.find('.caption, .title, .subtitle').css('transform', 'translateY(0)');
			}

		});

		// Various stuff that doesn't need remounting

		$(window).on('resize', function(){
			if ( $(window).width() < 1024 ) {

			}
		});

		$('.simple-grid select:not(.styled)').each(function(){
			$(this).styledSelect({
				coverClass: 'regular-select-cover',
				innerClass: 'regular-select-inner'
			}).addClass('styled');
			$(this).parent().append($.themeAssets.arrowDown);
		});

		if ( $('body').hasClass('template-customers-login') ) {

			if ( $('#RecoverPassword').length > 0 ) {

				$('#RecoverPassword').on('click', function(e){
					$('#CustomerLoginForm').hide();
					$('#RecoverPasswordForm').show();
					$('#LoginRecoverTitle').html($('#LoginRecoverTitle').data('password_recovery'));
					e.preventDefault();
				});

				$('#HideRecoverPasswordLink').on('click', function(e){
					$('#RecoverPasswordForm').hide();
					$('#CustomerLoginForm').show();
					$('#LoginRecoverTitle').html($('#LoginRecoverTitle').data('login'));
					e.preventDefault();
				});

				if (window.location.hash == '#recover') {
					$('#RecoverPassword').trigger('click');
				}

			}

		} else if ( $('body').hasClass('template-customers-addresses') ) {
			$('#section-addresses a').on('click', function(){
				window.CUBER.Scroll.mount();
			})
		}

		//

		if ( $('body').hasClass('template-blog') || $('body').hasClass('template-article') ) {

			window.CUBER.Main._mountScrollMovers({
				'parent': $('.scroll-movers-parent'),
				'items': $('.site-sharing')
			});

			window.CUBER.Main._mobileSharingInit();

		}

	});

	if ( $('body').attr('id') == 'challenge' ) {
		document.location.hash = '';
		$('html, body').scrollTop(0);
	}

})(jQuery);

function response(data) {
	console.log(data);
}

window._getLuminance = function(hexy) {
  var rgb = this._toRgb(hexy);
  return (rgb.r * 299 + rgb.g * 587 + rgb.b * 114) / 1000;
}

window._toRgb = function(hexy) {
	var hex = hexy.replace('rgb(', '');
		hex = hex.replace(')', '');
		hex = hex.replace(' ', '');
		hex = hex.replace(' ', '');
		hex = hex.split(',');
	return { r: hex[0], g: hex[1], b: hex[2] };
}
